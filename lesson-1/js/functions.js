
const baconator = createPizza("Medium", "Cheese, Bacon, Pineapple");
const cheesyPig = createPizza("Large", "Ham, Cheese");
const terminator = createPizza("Small", "Garlic, Cheese, Chilli");

const logger = function (message) {
    console.log(message + " " + Date.now());
};

logger("Baconator: " + baconator.price);
logger("KaasVark: " + cheesyPig.price);
logger("Terminator: " + terminator.price);


// Functions - Pure Function
function createPizza(size, toppings) {
  const pizza = {
    size: size,
    toppings: toppings,
    price: size.length + toppings.length + " Schmeckles",
  };

  return pizza; // Exposing
}