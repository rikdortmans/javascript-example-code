// https://randomuser.me/api?results=20

// JSON - JavaScript Object Notation
// JS - Single Threaded
// Event Loop - Browser

console.log("1. Before Timeout!");

setTimeout(function() {
    console.log("2. In Timeout!");
}, 0); // Event Loop - Browser

console.log("3. After Timeout!");

// EOF


// FETCH API!
// Fetch remote data over HTTP.


/** ENDPOINT/URL:
 * https://randomuser.me/api?results=20
 */

/** HEADERS
 * MIME Type text/html, text/css
 * Content Type: application/json
 */

/** HEADERS -> METHOD: VERBS: 
- GET: Read information from the server.
- POST: Add information to the server
- PUT. 
- PATCH. 
- DELETE. 
- OPTIONS
*/

const myPromise = new Promise(function(resolve, reject){
    // doSomething() very expensive.
    resolve(true);
});

const userListElement = document.getElementById("user-list")


console.log("1. before fetch");
// Defaults to GET
fetch("https://randomuser.me/api?results=2")

    .then(function(response) {
        console.log("3. Response: ", response);
        return response.json();
    })

    .then(function(json) {
        console.log("JSON: ", json);
        renderUsers(json.results);
    })

    .catch(function(error) {
        console.log(error);
    });



    function renderUsers(users) {
        for (const user of users) {
            // Template Literals - Interpolation
           userListElement.innerHTML += `
               <li>${user.name.first} - ${user.name.last}</li>
           `;
       }
    }


console.log("2. after fetch");