// ES6 Modules
import { users } from "../__mocks__/users.js";

console.log(users);

// HOF
/** ONLY ON ARRAYS!
 * - Filter - Always returns an array. - Changes the length
 * - Map - Always return an array. - NEVER! changes the length
 * - Find - Get 1 specific result (The Object!)
 * - FindIndex - Get 1 specific result's index
 */
const GENDER_MALE = "male";
const GENDER_FEMALE = "female";

const males = users.filter((user) => user.gender === GENDER_MALE);
const females = users.filter((user) => user.gender === GENDER_FEMALE);

// 1. Remove the word function.
// 2. Replace function  with => on the right of ()
// 3. Remove useless code
console.log(males);
console.log(females);

// Map
const telemarketers = users.map(function (user) {
  // Return - New data structure.
  const cell = user.cell;
  const {
    name: { first, last, title },
  } = user;
  // const name = user.name.title + " " + user.name.first + " " + user.name.last;
  // Object Literals
  return {
    cell,
    name: title + " " + first + " " + last,
  };
});
console.log("MAP RESULT: ");
console.log(telemarketers);

// cell
// name (title first and last)

const cellNumbers = users.map((user) => user.cell);
console.log("MAP RESULT (cell numbers): ");
console.log(cellNumbers);

// Find
const theButler = telemarketers.find(
  user => user.name === "Mr Zachary Butler"
);
console.log("FIND RESULT: ");
console.log(theButler);

function findMatch(value, query) {
    return value.toLowerCase().includes(query)
}

// Searching with the Filter
const query = "Bu".toLowerCase();
const searchResults = users.filter(user => {
    if (findMatch(user.name.first, query)) {
        return true;
    }
    if (findMatch(user.name.last, query)) {
        return true;
    }
    return false;
});

console.log("Search Results");
console.log(searchResults);